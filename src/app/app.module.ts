import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";
import { HttpClientModule } from '@angular/common/http';

import { appRoutes } from './routes';

import { ChatService } from './service/chat.service';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ChatComponent } from './chat/chat.component';
import { MessageComponent } from './message/message.component';
import { MessageItemComponent } from './message-item/message-item.component';
import { FormChatComponent } from './form-chat/form-chat.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ChatComponent,
    MessageComponent,
    MessageItemComponent,
    FormChatComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    ChatService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

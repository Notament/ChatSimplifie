import { Component, OnInit } from '@angular/core';
import { ChatService } from '../service/chat.service';
import { Message } from '../model/Message';
@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  messages: Message

  constructor(private chatService: ChatService) { }

  ngOnInit() {

    this.chatService.messagesSubject.subscribe(
      (messages) => this.messages = messages
    )
    this.chatService.getMessages();
  }

}

import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Message } from "../model/Message";
import { Subject } from 'rxjs/Subject';


@Injectable()
export class ChatService {

    //private api: string = 'http://192.168.1.44/PROJETS/ultimeSimpleChat/chatAPI/public';
    private api: string = 'http://192.168.1.37/ultimatesimplechat/chatAPI/public';
    private array_message: Message[] = [];

    public messagesSubject:Subject<any>  = new Subject<any>()

    constructor(private http: HttpClient) {
    }

    emitMessages() {
        this.messagesSubject.next(this.array_message);
    }

    getMessages() {

        let observable = this.http.get<any>(`${this.api}/room`);
        let observer = {
            next: (data)=> {
                this.array_message = [];
                data = JSON.parse(data);
                data.forEach((element) => {
                    this.array_message.push(new Message(element.id, element.User, element.Content))
                });
                this.emitMessages();
            },
            error: (error)=> console.log(),
            complete: ()=> console.log(),
          }
        observable.subscribe(observer);
    }

    addMessage(pseudo,message) {
        let observable = this.http.post<any>(`${this.api}/message/new`, {'user':pseudo, 'content':message});
        let observer = {
            next: (data)=> {
                this.getMessages()
            },
            error: (error)=> console.log(),
            complete: ()=> console.log(),
          }
        observable.subscribe(observer);
    }
}
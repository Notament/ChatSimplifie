import { Routes } from "@angular/router";
import { ChatComponent } from "./chat/chat.component";

export const appRoutes: Routes = [
    { path:"**", component: ChatComponent}
  ]
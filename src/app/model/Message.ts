export class Message{

    id: number;
    pseudo: string;
    message: string;

    constructor(id, pseudo, message){
        this.id = id;
        this.pseudo = pseudo;
        this.message = message;
    }
}

import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ChatService } from '../service/chat.service';

@Component({
  selector: 'app-form-chat',
  templateUrl: './form-chat.component.html',
  styleUrls: ['./form-chat.component.scss']
})
export class FormChatComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private chatService: ChatService) { }

  addForm: FormGroup;

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.addForm = this.formBuilder.group({
      pseudo: ['', Validators.required],
      message: ['', Validators.required],
    });
  }

  addMessage() {
    const pseudo = this.addForm.get('pseudo').value;
    const message = this.addForm.get('message').value;
    this.chatService.addMessage(pseudo,message);
  }

}
